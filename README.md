# README #

AUTO-TWEETY-BOT is a python bot , that writtes your set tweets from a textfile, automaticly every 15 minutes , allowing you to be active.

### Requirements: ###

* Python 2.7
* Python lib : Coverage
* Python lib : Requests
* Python lib : Requests-coveralls
* Python lib : Nose-cov
* Python lib : Responses

* Go to the script folder and install using this command : easy_install twython  | or |  pip install twython 

### How to use: ###

* Example : scriptname.py textfile.txt
